/**
 * MainController
 *
 * @description :: Server-side logic for managing mains
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var MainController =  {
	index: function(req, res){
	    res.view();
	},
	
	signup: function(req, res){

	},

	login: function(req, res){

	},

	chat: function(req, res){

	}
};


module.exports = MainteController;
